﻿using Innergy.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Innergy.Services
{
    public static class SerializerService
    {
        public static IEnumerable<Warehouse> SerializeWarehouseObject(this string model)
        {
            var rows = Regex.Split(model, "\r\n|\r|\n");
            var warehouseList = new List<Warehouse>();
            foreach (var item in rows)
            {
                var nonSerializedMaterial = item.Split(";");
                if (nonSerializedMaterial.Length > 2)
                {
                    var materialName = nonSerializedMaterial[0].Trim();
                    var materialId = nonSerializedMaterial[1].Trim();

                    var material = new Material(materialName, materialId);

                    var nonSerializedWarehousesWithMaterialAmount = nonSerializedMaterial[2].Split("|");
                    foreach (var nonSerializedWarehouseWithMaterialAmount in nonSerializedWarehousesWithMaterialAmount)
                    {
                        var nonSerializedWarehouse = nonSerializedWarehouseWithMaterialAmount.Split(",");
                        var warehouseName = nonSerializedWarehouse[0].Trim();
                        var amount = int.Parse(nonSerializedWarehouse[1].Trim());

                        var stock = new Stock(material, amount);

                        var warehouse = CreateWarehouse(warehouseName, warehouseList);
                        warehouse.AddStock(stock);
                        AddWarehouseToList(warehouse, ref warehouseList);
                    }
                }
            }
            return warehouseList;
        }

        private static Warehouse CreateWarehouse(string warehouseName, List<Warehouse> warehouses)
        {
            if (warehouses.Any(x => x.WarehouseName == warehouseName))
            {
                return warehouses.FirstOrDefault(x => x.WarehouseName == warehouseName);

            }
            return new Warehouse(warehouseName);
        }

        private static void AddWarehouseToList(Warehouse warehouse, ref List<Warehouse> warehouses)
        {
            if (!warehouses.Any(x => x.WarehouseName == warehouse.WarehouseName))
            {
                warehouses.Add(warehouse);
            }
        }
    }
}
