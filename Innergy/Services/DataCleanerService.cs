﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Innergy.Services
{
    public static class DataCleanerService
    {
        public static string CleanModelOfUnncessaryLines(this string model)
        {
            var rows = Regex.Split(model, "\r\n|\r|\n");
            var cleanedModel = rows.Where(x => !x.Trim().StartsWith("#"));
            return string.Join("\n", cleanedModel);
        }
    }
}
