﻿using Innergy.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Services
{
    public static class DisplayerService
    {
        public static void DisplaySortedInventory(this IEnumerable<Warehouse> warehouses)
        {
            StringBuilder info = new StringBuilder();
            foreach (var warehouse in warehouses.OrderByDescending(x => x.Stocks.Sum(x => x.Amount)).ThenByDescending(x => x.WarehouseName))
            {
                info.Append($"{warehouse.WarehouseName} (total {warehouse.Stocks.Sum(x => x.Amount)})\n");
                foreach (var material in warehouse.Stocks.OrderBy(x => x.Material.MaterialId))
                {
                    info.Append($"{material.Material.MaterialId}: {material.Amount}\n");
                }
                info.Append(Environment.NewLine);
            }
            Console.WriteLine(info);
        }
    }
}
