﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Domain
{
    public class Warehouse
    {
        public Warehouse(string warehouseName)
        {
            WarehouseName = warehouseName;
            Stocks = new List<Stock>();
        }
        public void AddStock(Stock stock)
            => Stocks.Add(stock);
        public string WarehouseName { get; private set; }
        public int Amount { get; private set; }
        public List<Stock> Stocks { get; private set; }
    }
}
