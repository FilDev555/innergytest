﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Domain
{
    public class Stock
    {
        public Stock(Material material, int amount)
        {
            Material = material;
            Amount = amount;
        }
        public int Amount { get; private set; }

        public Material Material { get; private set; }
    }
}
