﻿
namespace Innergy.Domain
{
    public class Material
    {
        public Material(string materialName, string materialId)
        {
            MaterialName = materialName;
            MaterialId = materialId;
        }
        public string MaterialName { get; private set; }
        public string MaterialId { get; private set; }

    }
}
